import { all, fork } from "redux-saga/effects";
import { watchAppLoad } from "../state/app/services";

export default function* root() {
  yield all([fork(watchAppLoad)]);
}
