import axios from "axios";

export function* callAPI({ url = "", method = "get", data = {}, params = {} }) {
  const config = {
    method,
    data,
    params,
    url: `${baseUrl}/${url}`,
    headers: {
      "content-type": "application/json"
    }
  };
  return yield axios(config);
}
