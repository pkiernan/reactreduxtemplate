import React, { Component } from "react";

class HomePage extends Component {
  render() {
    return (
      <section className="page home-page">
        <h1>Home Page</h1>
      </section>
    );
  }
}
export default HomePage;
