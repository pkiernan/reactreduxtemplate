import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import {
  selectorIsLoaded,
  selectorIsLoading,
  selectorWelcomeMessage
} from "../../state/app/selectors";
import { actionLoadApp } from "../../state/app/actions";
import "./styles.scss";

class AppHeaderComponent extends Component {
  static propTypes = {
    isLoaded: PropTypes.bool.isRequired,
    isLoading: PropTypes.bool.isRequired,
    loadApp: PropTypes.func.isRequired,
    welcomeMessage: PropTypes.string
  };
  static defaultProps = {
    welcomeMessage: ""
  };
  componentDidMount() {
    const { loadApp } = this.props;
    loadApp();
  }
  renderLoading() {
    const { isLoaded, isLoading } = this.props;
    return !isLoaded && isLoading ? "loading" : null;
  }
  renderMessage() {
    const { welcomeMessage } = this.props;
    return <p>{welcomeMessage}</p>;
  }
  renderContent() {
    const { isLoaded } = this.props;
    return isLoaded ? (
      <Fragment>
        {this.renderMessage()}
        <nav className="app-nav">
          <Link to="/">Home Page</Link>
          <Link to="/detail">Detail Page</Link>
        </nav>
      </Fragment>
    ) : null;
  }
  render() {
    return (
      <header className="app-header">
        <h1>Template</h1>
        {this.renderLoading()}
        {this.renderContent()}
      </header>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  loadApp: () => dispatch(actionLoadApp.call())
});

const mapStateToProps = state => ({
  isLoading: selectorIsLoading(state),
  isLoaded: selectorIsLoaded(state),
  welcomeMessage: selectorWelcomeMessage(state)
});

export default connect(mapStateToProps, mapDispatchToProps)(AppHeaderComponent);
