import AppHeader from "../AppHeader";
import { LOAD_APP } from "../../../state/app/types";
import { actionLoadApp } from "../../../state/app/actions";
import { runComponentIntegrationTests } from "../../../utils/test";

const successMessage = "testing message success";

const config = {
  id: "AppHeader",
  Component: AppHeader,
  actions: [
    {
      id: LOAD_APP.CALL,
      action: actionLoadApp.call,
      expectedProps: {
        isLoading: true,
        isLoaded: false,
        welcomeMessage: ""
      }
    },
    {
      id: LOAD_APP.FAIL,
      action: actionLoadApp.fail,
      expectedProps: {
        isLoading: false,
        isLoaded: false,
        welcomeMessage: ""
      }
    },
    {
      id: LOAD_APP.SUCCESS,
      action: actionLoadApp.success,
      payload: {
        data: {
          message: successMessage
        }
      },
      expectedProps: {
        isLoading: false,
        isLoaded: true,
        welcomeMessage: successMessage
      }
    }
  ]
};
runComponentIntegrationTests(config);
