import { createStore, applyMiddleware, compose } from "redux";
import { defaultsDeep } from "lodash";
import rootReducer from "../state";
import createSagaMiddleware from "redux-saga";
import sagas from "../sagas";

const sagaMiddleware = createSagaMiddleware();
const composeEnhancers = global.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; // eslint-disable-line

const setInitialState = (state = {}) => (defaultState, action) => {
  return Object.keys(state).length
    ? rootReducer(defaultState, action)
    : rootReducer(defaultsDeep(state, defaultState), action);
};

export const createStoreWithState = (state = {}, isMocked = false) => {
  const store = createStore(
    setInitialState(state),
    composeEnhancers(applyMiddleware(sagaMiddleware))
  );
  if (!isMocked) {
    sagaMiddleware.run(sagas);
  }
  return store;
};
