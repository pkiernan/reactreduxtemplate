import { createSelector } from "reselect";

const getState = ({ app = {} }) => app;

export const selectorWelcomeMessage = createSelector(
  getState,
  ({ welcomeMessage = "" }) => welcomeMessage || ""
);

export const selectorIsLoading = createSelector(
  getState,
  ({ isLoading }) => isLoading === true
);

export const selectorIsLoaded = createSelector(
  selectorWelcomeMessage,
  selectorIsLoading,
  (welcomeMessage, isLoading) =>
    isLoading === false && welcomeMessage.length > 0
);
