import { LOAD_APP } from "./types";

const initState = {
  isLoading: false,
  welcomeMessage: ""
};

export default (state = initState, action) => {
  const { type, payload = {} } = action;
  switch (type) {
    case LOAD_APP.CALL: {
      return {
        ...state,
        isLoading: true
      };
    }
    case LOAD_APP.SUCCESS: {
      return {
        ...state,
        isLoading: false,
        welcomeMessage: payload.data.message
      };
    }
    case LOAD_APP.FAIL: {
      return {
        ...state,
        isLoading: false,
        welcomeMessage: initState.welcomeMessage
      };
    }
    default:
      return state;
  }
};
