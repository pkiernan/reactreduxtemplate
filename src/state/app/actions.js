import { LOAD_APP } from "./types";
import { createAction } from "../../utils/actions";
import { ACTION_API } from "../../constants";

export const actionLoadApp = createAction({ id: LOAD_APP, type: ACTION_API });
