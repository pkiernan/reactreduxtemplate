import { LOAD_APP } from "../types";
import { actionLoadApp } from "../actions";
import * as selectors from "../selectors";
import { runStateIntegrationTests } from "../../../utils/test";

const successMessage = "testing message success";

const config = {
  id: "app",
  selectors,
  defaultValues: {
    isLoading: null,
    welcomeMessage: null
  },
  actions: [
    {
      id: LOAD_APP.CALL,
      action: actionLoadApp.call,
      expectedValues: {
        selectorWelcomeMessage: "",
        selectorIsLoaded: false,
        selectorIsLoading: true
      }
    },
    {
      id: LOAD_APP.SUCCESS,
      action: actionLoadApp.success,
      payload: {
        data: {
          message: successMessage
        }
      },
      expectedValues: {
        selectorWelcomeMessage: successMessage,
        selectorIsLoaded: true,
        selectorIsLoading: false
      }
    },
    {
      id: LOAD_APP.FAIL,
      action: actionLoadApp.fail,
      expectedValues: {
        selectorWelcomeMessage: "",
        selectorIsLoaded: false,
        selectorIsLoading: false
      }
    }
  ]
};

runStateIntegrationTests(config);
