import React from "react";
import { Switch, Route } from "react-router-dom";
import { AppHeader } from "./components/AppHeader";
import { DetailPage } from "./pages/Detail";
import { HomePage } from "./pages/Home";
import "./styles/app.scss";

function App() {
  return (
    <div className="App">
      <AppHeader />
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/detail" component={DetailPage} />
      </Switch>
    </div>
  );
}

export default App;
