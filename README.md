### Template Information

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app) and extended by [Paul Kiernan](<[https://bitbucket.org/pkiernan](https://bitbucket.org/pkiernan)>).

#### Available Scripts

In the project directory, you can run:

### `npm start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`

Launches the test runner in the interactive watch mode.

### `npm run build`

Builds the app for production to the `build` folder.

### `npm run server`

Serves a simple db file via json-server.

#### Technologies

- React
- Redux
- Redux-Saga
- Reselect
- Axios
- Jest
- Enzyme
- Json-Server
